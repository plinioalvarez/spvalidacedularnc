/* Procedure Realizado por Plinio Alvarez */
CREATE FUNCTION [dbo].[VALIDARNC](@RNC VARCHAR(11))
RETURNS CHAR(1)
AS
BEGIN
  DECLARE
       @A int = 0,
       @B int = 0,
       @R varchar(2) = '',
       @CNT int = 0,
       @TablaCedula varchar(10) = '1212121212',
       @TablaRNC varchar(8) = '79865432',
       @Suma int =0,
       @division int = 0,
       @DigitoVerif int = '',
       @result char(1);
     
  IF ISNUMERIC(@RNC) = 1 OR RTRIM(LTRIM(@RNC)) = ''
     BEGIN
       IF LEN(@RNC) = 9
          BEGIN    
            WHILE @CNT < 8
              Begin
                SET @cnt = @cnt + 1
                SET @Suma = @Suma + (convert(varchar(2),convert(float,SUBSTRING(@RNC,@CNT,1)) * 
                                     convert(float, SUBSTRING(@TablaRNC,@CNT,1))))
              END
              
              SET @Division = @suma -((@Suma / 11)*11);
              
              if @Division = 0 
                 set @DigitoVerif = 2
               else  
              if @Division = 1 
                 set @DigitoVerif = 1
              else
                set @DigitoVerif = 11 - @Division

              if @DigitoVerif = substring(@RNC,9,1) 
                  set @result = 'S'
                  else
                  set @result = 'N'
              END
            ELSE
              IF LEN(@RNC) = 11
                 BEGIN
                   WHILE @CNT < 10
                     BEGIN
                       SET @cnt = @cnt + 1
                       SET @R = convert(varchar(2),convert(float,SUBSTRING(@RNC,@CNT,1)) * 
                                             convert(float, SUBSTRING(@TablaCedula,@CNT,1)))
                       IF LEN(@R) > 1
                          BEGIN
                            SET @A = substring(@R,1,1)
                            SET @B = substring(@R,2,1)
                          END
                       ELSE
                         BEGIN
                           SET @A = 0
                           SET @B = substring(@R,1,1)
                         END
                       
                       SET @Suma = @Suma + @A + @B
                     END
                      
                   SET @Division = (@Suma / 10) * 10
                   
                   IF @Division < @Suma 
                      SET @Division = @Division + 10
                      
                   SET @DigitoVerif = @Division - @Suma;
                   
                   IF @DigitoVerif = substring(@RNC,11,1) 
                      SET @result = 'S'
                   ELSE
                     SET @result = 'N'

              END
            ELSE
              SET @RESULT = 'N'
      END
    ELSE
      SET @RESULT = 'N'
 
  IF SUBSTRING(@RNC,1,4) = '0000'
     SET @RESULT = 'N' 


  RETURN @result
 
END;
